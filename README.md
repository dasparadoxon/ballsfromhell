# BALLS FROM HELL #

"Balls from Hell" is a little game experiment in Unity3D.

![ballsFromHell_SpecialBar.jpg](https://bitbucket.org/repo/kpbRb6/images/2962113820-ballsFromHell_SpecialBar.jpg)



An ancient legend has come true and everywhere in the land mountains appear out of nowhere.
They spit big rocks who threatend to destroy the villages and landscape around them.

Your job is to prevent damage to the villages and capture all balls before they reach the ground.

You are a hero.

# MEDIA #

[Gameplay - Video on YouTube](https://www.youtube.com/watch?v=o09QS8mIras)

# Version #

**Version 0.0971**

# Installation #

Just import the project into Unity.

# Credits #

Comming soon.

# License #

This is until further notice under [Attribution-NonCommercial-ShareAlike 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/us/).