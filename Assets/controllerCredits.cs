﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class controllerCredits : MonoBehaviour {

	private bool debug = true;

	void Start () {
	
	}

	void Update () {
	
	}

	/// <summary>
	/// Switchs back to main menu.
	/// 
	/// Called from the button in the UI
	/// </summary>
	public void switchBackToMainMenu(){

		if (debug)
			Debug.Log ("Switching from Credits to Main Menu...");

		SceneManager.LoadScene(0); 

	}
}
