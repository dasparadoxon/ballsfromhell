﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Player
{

	/// <summary>
	/// The points.
	/// </summary>
	public int points = 0;

	/// <summary>
	/// Gets or sets the points.
	/// </summary>
	/// <value>The points.</value>
	public int Points {
		get {
			return this.points;
		}
		set {
			points = value;
		}
	}
}


public class Spawner : ScriptableObject
{

	Rect spawningArea;
	List<Rect> excludedAreas;
	GameObject prefabToSpawn;
	int numberOfObjectsToSpawn;
	string parentFolderName;

	/// <summary>
	/// Initializes a new instance of the <see cref="Spawner"/> class.
	/// </summary>
	/// <param name="size">Size.</param>
	/// <param name="position">Position.</param>
	/// <param name="numberToSpawn">Number to spawn.</param>
	/// <param name="whichPrefab">Which prefab.</param>
	/// <param name="newParentFolderName">New parent folder name.</param>
	public Spawner (Vector2 size, Vector2 position, int numberToSpawn, GameObject whichPrefab, string newParentFolderName)
	{

		parentFolderName = newParentFolderName;

		spawningArea = new Rect (position, size);
		excludedAreas = new List<Rect> ();
		numberOfObjectsToSpawn = numberToSpawn;
		prefabToSpawn = whichPrefab;

	}

	/// <summary>
	/// Adds the area to exclude.
	/// </summary>
	/// <param name="size">Size.</param>
	/// <param name="position">Position.</param>
	public void addAreaToExclude (Vector2 size, Vector2 position)
	{

		Rect newAreaToExclude = new Rect (size, position);
		excludedAreas.Add (newAreaToExclude);
	}

	/// <summary>
	/// Spawn this instance.
	/// </summary>
	public void Spawn ()
	{

		int counter = 0;

		GameObject spawnParent = new GameObject ();

		spawnParent.name = parentFolderName;

		while (counter < numberOfObjectsToSpawn) {

			Vector3 positionToSpawn = new Vector3 (
				                          spawningArea.center.x + Random.Range (-spawningArea.size.x / 2f, spawningArea.size.x / 2f),
				                          0.161f, spawningArea.center.y + Random.Range (-spawningArea.size.x / 2f, spawningArea.size.x / 2f));

			GameObject spawnObject = Instantiate (prefabToSpawn, positionToSpawn, Quaternion.identity) as GameObject;

			spawnObject.transform.Rotate (-90, 0, 0);

			spawnObject.transform.parent = spawnParent.transform;

			counter++;
		}

	}

}

public class controllerGame : MonoBehaviour
{

	[Header ("Controller of other Objects")]
	/// <summary>
	/// The ball spawner.
	/// </summary>
	public controllerBallSpawner ballSpawner;

	public ControllerApplication applicationController;
	/// <summary>
	/// The bowl.
	/// </summary>
	public controllerBowl bowl;
	/// <summary>
	/// The floor controller.
	/// </summary>
	public controllerFloor floorController;

	[Header ("UI Text Elements")]
	/// <summary>
	/// The seconds intro text.
	/// </summary>
	public Text secondsIntroText;
	/// <summary>
	/// The seconds live counter.
	/// </summary>
	public Text secondsLiveCounter;
	/// <summary>
	/// The points live counter.
	/// </summary>
	public Text pointsLiveCounter;
	/// <summary>
	/// The game over points text.
	/// </summary>
	public Text gameOverPointsText;

	[Header ("UI Canvases")]
	/// <summary>
	/// The intro canvas.
	/// </summary>
	public GameObject introCanvas;
	/// <summary>
	/// The running canvas.
	/// </summary>
	public GameObject runningCanvas;
	/// <summary>
	/// The game over canvas.
	/// </summary>
	public GameObject gameOverCanvas;
	/// <summary>
	/// The menu canvas.
	/// </summary>
	public GameObject menuCanvas;
	/// <summary>
	/// The tutorial canvas.
	/// </summary>
	public GameObject tutorialCanvas;

	[Header ("Prefabs")]
	/// <summary>
	/// The forest tree prefab.
	/// </summary>
	public GameObject forestTreePrefab;
	/// <summary>
	/// The dead forest tree prefab.
	/// </summary>
	public GameObject deadForestTreePrefab;
	/// <summary>
	/// The forest gras prefab.
	/// </summary>
	public GameObject forestGrasPrefab;
	/// <summary>
	/// The village prefab.
	/// </summary>
	public GameObject villagePrefab;


	[Header ("Options")]
	/// <summary>
	/// The time till game over.
	/// </summary>
	public float timeTillGameOver = 5.0f;

	[Header ("Game Modes")]
	/// <summary>
	/// The show menu.
	/// </summary>
	public bool showMenu = false;
	/// <summary>
	/// The level mode.
	/// </summary>
	public bool levelMode = false;
	/// <summary>
	/// The show tutorials.
	/// </summary>
	public bool showTutorials = true;

	[Header ("Debug")]

	/// <summary>
	/// The vertices mark debug.
	/// </summary>
	public GameObject verticesMarkDebug;
	/// <summary>
	/// The current level number.
	/// </summary>
	public int currentLevelNumber = 0;
	/// <summary>
	/// The is game running.
	/// </summary>
	public bool isGameRunning = false;

	public bool showBriefing = true;

	public bool debugSpawnVillages = true;

	public List<Special> activeSpecials;

	public int numberOfBalls = 0;

	// PRIVATE MEMBERS

	// REMEMBER :
	// BRIEFING IS FOR LEVEL MODE
	// TUTORIAL FOR ENDLESS MODE

	bool levelRunning = false;
	bool levelFinished = false;
	bool briefingRunning = false;
	bool gameIsOver = false;
	bool tutorialOver = false;
	bool tutorialShown = false;

	Player player;

	int[] secondsPerLevel = new int[] { 10, 20, 30, 40, 50 };
	int[] villageAreaSquarePerLevel = new int[] { 10, 10, 10, 10, 10 };
	int[] villagesPerLevel = new int[] { 1, 3, 5, 10, 20 };
	float[] spawnTimePerLevel = new float[] { 1.0f, 0.5f, 0.3f, 0.2f, 0.1f };

	float secondsUntilLevelFinished = 5f;
	bool nonLevelModeInitialized = false;

	Mesh orginalFloorMesh;

	Spawner treeSpawner;
	Spawner deadTreeSpawner;
	Spawner forestGrasSpawner;

	int burningVillages = 0;

	bool debug = true;

	void spawnVillages (int numberOfVillages)
	{

		for (int i = 0; i < numberOfVillages; i++)
			spawnVillage ();

	}

	void spawnVillage ()
	{

		float villageSpawnRange = (float)villageAreaSquarePerLevel [currentLevelNumber];

		Vector3 ballSpawnerPos = ballSpawner.transform.position;

		float spawnerOffset = 7;


		Vector3 posToInstantiate = new Vector3 (Random.Range (-villageSpawnRange, villageSpawnRange), 0, Random.Range (-villageSpawnRange, villageSpawnRange));

		if (posToInstantiate.x >= 0 && posToInstantiate.x < spawnerOffset)
			posToInstantiate.x = spawnerOffset;

		if (posToInstantiate.x < 0 && posToInstantiate.x > -spawnerOffset)
			posToInstantiate.x = -spawnerOffset;

		if (posToInstantiate.z >= 0 && posToInstantiate.z < spawnerOffset)
			posToInstantiate.z = spawnerOffset;

		if (posToInstantiate.z < 0 && posToInstantiate.z > -spawnerOffset)
			posToInstantiate.z = -spawnerOffset;

		posToInstantiate = posToInstantiate + new Vector3 (ballSpawnerPos.x, 0, ballSpawnerPos.z);

		Instantiate (villagePrefab, posToInstantiate, Quaternion.identity);

	}

	void Reset ()
	{

		ballSpawner.deactivate ();

		levelRunning = false;
		levelFinished = false;
		briefingRunning = false;

		isGameRunning = false; 
	}

	void Start ()
	{

		applicationController = setApplicationControllerFromSingelton();

		setLevelMode(applicationController.levelMode);

		Debug.Log ("Level Mode is :" + levelMode);

		activeSpecials = new List<Special> ();

		player = new Player ();

		secondsUntilLevelFinished = secondsPerLevel [currentLevelNumber];

		// Spawn landscape objects

		treeSpawner = new Spawner (new Vector2 (110, 110), new Vector2 (-145f, -20f), 400, forestTreePrefab, "Forest Trees");
		treeSpawner.Spawn ();

		deadTreeSpawner = new Spawner (new Vector2 (110, 110), new Vector2 (-145f, -20f), 150, deadForestTreePrefab, "Dead Forest Trees");
		deadTreeSpawner.Spawn ();

		forestGrasSpawner = new Spawner (new Vector2 (110, 110), new Vector2 (-145f, -20f), 150, forestGrasPrefab, "Forest Grass");
		forestGrasSpawner.Spawn ();




	}

	private void setLevelMode(bool levelModeStatus){

		levelMode = levelModeStatus;


	}

	void missionBriefing ()
	{

		briefingRunning = true;

		GameObject goc = (GameObject)introCanvas;

		goc.SetActive (true);

		secondsIntroText.text = secondsUntilLevelFinished.ToString ();

	}

	/// <summary>
	/// Briefings the done.
	/// </summary>
	public void briefingDone ()
	{

		if (debugSpawnVillages)
			spawnVillages (villagesPerLevel [currentLevelNumber]);

		levelRunning = true;

		bowl.active = true;

		GameObject goc = (GameObject)introCanvas;

		goc.SetActive (false);

		secondsLiveCounter.text = secondsUntilLevelFinished.ToString ();

		runningCanvas.SetActive (true);

		isGameRunning = true;

	}

	public ControllerApplication setApplicationControllerFromSingelton(){

		if (debug)
			Debug.Log ("Main Menu Controller gets Application Controller from Singelton...");

		GameObject singeltonApplicationController =  GameObject.Find ("ApplicationController");

		ControllerApplication cApp = singeltonApplicationController.GetComponent<ControllerApplication> ();

		return cApp;
	}


	void Update ()
	{

		// LEVEL MODE

		if (levelMode) {

			if (!levelRunning && !levelFinished && !briefingRunning && !gameIsOver) {

				missionBriefing ();

				if (!showBriefing)
					briefingDone ();
			}

			if (levelRunning && briefingRunning) {

				briefingRunning = false;

				ballSpawner.spawnIntervalInSeconds = spawnTimePerLevel [currentLevelNumber];

				ballSpawner.activate ();

			}

			if (levelRunning) {

				secondsUntilLevelFinished -= Time.deltaTime;

				int secondsUntilLevelFinishedInt = (int)secondsUntilLevelFinished;

				// Update UI 

				secondsLiveCounter.text = secondsUntilLevelFinishedInt.ToString ();
				pointsLiveCounter.text = player.Points.ToString ();

				if (secondsUntilLevelFinished < 0.6 && burningVillages == 0) {

					removeAllComets ();

					runningCanvas.SetActive (false);
				
					levelRunning = false;

					currentLevelNumber++;

					if (currentLevelNumber > 4)
						currentLevelNumber = 0;

					secondsUntilLevelFinished = secondsPerLevel [currentLevelNumber];

					levelFinished = true;

					floorController.restoreFloor ();

					burningVillages = 0;



				}

			}

			if (levelFinished) {

				Reset ();

			}


			// set by the gameOver method which is called bei the village ruins controller
			// after the village burn sequenz is over

			if (gameIsOver) {

				gameOverPointsText.text = player.Points.ToString ();

				bowl.active = false;

				// Since the Highscore Menu is working now, we transfer the result score
				// to the Application Singelton Controller and load the HighScore Menu

				// => restore to use the game over menu in this scene
				//gameOverCanvas.SetActive (true);
				//Reset ();

				int newHighscore = player.Points;

				loadHighScoreSceneWithScore (newHighscore);

			}

		}

		// show the tutorial canvas for non-level (endless) loop
		// the button on the canvas will switch tutorial 

		if (!tutorialOver && !levelMode && !tutorialShown) {

			tutorialShown = true;

			tutorialCanvas.SetActive (true);
		}

		// both handling the initialisation and running for non-level (endless) mode

		if (tutorialOver) {

			if (!levelMode && !nonLevelModeInitialized) {

				tutorialCanvas.SetActive (false);

				ballSpawner.activate ();
				bowl.active = true;
				nonLevelModeInitialized = true;
				spawnVillages (15);

				secondsUntilLevelFinished = 0;

				// set up nonLevelMode UI

				runningCanvas.SetActive (true);

				isGameRunning = true;



			}
			if (!levelMode && nonLevelModeInitialized) {
			
				secondsUntilLevelFinished += Time.deltaTime;

				int secondsUntilLevelFinishedInt = (int)secondsUntilLevelFinished;

				// Update UI 

				secondsLiveCounter.text = secondsUntilLevelFinishedInt.ToString ();
				pointsLiveCounter.text = player.Points.ToString ();

			}

		}

		handleActiveSpecials ();

		input ();


	
	}

	void loadHighScoreSceneWithScore(int resultScore){

		applicationController.score = resultScore;

		if (debug)
			Debug.Log ("Loading HighScore Scene with score : "+resultScore);

		SceneManager.LoadScene(2); 

	}


	void input ()
	{

		if (Input.GetMouseButtonDown (1)) {

			player.points = player.points + 100;

		}

		if (Input.GetMouseButtonDown (2)) {

			if (debug)
				Debug.Log ("Second Mouse button for debug game over pressed.");

			gameIsOver = true;

		}

	}

	/// <summary>
	/// Games the over.
	/// </summary>
	public void gameOver ()
	{

		if (!briefingRunning)
			gameIsOver = true;

		removeUnwantedObjectsForRestart ();

		removeAllComets ();

		floorController.restoreFloor ();

	}



	private void removeUnwantedObjectsForRestart ()
	{

		GameObject[] removeAfterRestart = GameObject.FindGameObjectsWithTag ("RemoveAfterRestart");

		foreach (GameObject toRemove in removeAfterRestart) {
			Destroy (toRemove);
		}

	}

	/// <summary>
	/// Removes all comets.
	/// </summary>
	public void removeAllComets ()
	{

		GameObject[] removeAfterRestart = GameObject.FindGameObjectsWithTag ("Comet");

		foreach (GameObject toRemove in removeAfterRestart) {
			Destroy (toRemove);
		}
	}

	/// <summary>
	/// Restart this instance.
	/// </summary>
	public void restart ()
	{

		gameOverCanvas.SetActive (false);
		Reset ();
		gameIsOver = false;

		currentLevelNumber = 0;

		player.Points = 0;

		secondsUntilLevelFinished = secondsPerLevel [currentLevelNumber];

	}

	/// <summary>
	/// Leaves the game.
	/// </summary>
	public void leaveGame ()
	{

		Application.Quit ();
	}

	/// <summary>
	/// Adds the points.
	/// </summary>
	/// <param name="numberOfPoints">Number of points.</param>
	public void addPoints (int numberOfPoints)
	{

		player.Points += numberOfPoints;
	}

	/// <summary>
	/// Ends the menu.
	/// </summary>
	public void endMenu ()
	{

		showMenu = false;

	}

	/// <summary>
	/// Adds the burning village.
	/// </summary>
	public void addBurningVillage ()
	{

		burningVillages++;
	}

	/// <summary>
	/// Removes the burning village.
	/// </summary>
	public void removeBurningVillage ()
	{

		burningVillages--;
	}

	/// <summary>
	/// Tutorials the is over.
	/// </summary>
	public void tutorialIsOver ()
	{

		tutorialOver = true;
	}

	/// <summary>
	/// Games the status.
	/// </summary>
	/// <returns><c>true</c>, if status was gamed, <c>false</c> otherwise.</returns>
	public bool gameStatus ()
	{

		return isGameRunning;
	}

	/// <summary>
	/// Handles the activated specials
	/// the list is updated in the specials controller
	/// </summary>
	public void handleActiveSpecials ()
	{

		if (activeSpecials.Count > 0) {
			
			foreach (Special special in activeSpecials.ToArray()) {

				//right now the special class registeres itself in the specials controller manually
				special.Action ();
			
			}

		}

	}

	/// <summary>
	/// Adds a ball to the ball counter
	/// </summary>
	public void ballCreated(){

		numberOfBalls++;

	}

	public void ballDestroyed(){

		numberOfBalls--;

	}

}
