﻿using UnityEngine;
using System.Collections;

public class controllerBowlBottomCollider : MonoBehaviour {

	controllerGame gameController;

	public GameObject cometDestroyment;

	AudioSource zapping;

	// Use this for initialization
	void Start () {

		zapping = GetComponent<AudioSource> ();

		GameObject gameObject =  GameObject.FindGameObjectWithTag ("GameController");

		gameController = gameObject.GetComponent<controllerGame> ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void onCollisionEnter(){

	}

	void OnCollisionEnter(Collision collision) {

		string collisionObjectName = collision.collider.gameObject.name;
		string collisionObjectTag = collision.collider.gameObject.tag;

		//Debug.Log (collisionObjectTag);

		if (collisionObjectTag == "Comet") {

			Destroy (collision.collider.gameObject);

			//GameObject zappingParticleSystem = Instantiate (cometDestroyment, collision.collider.transform.position, Quaternion.identity) as GameObject;

			GameObject zappingParticleSystem = Instantiate (cometDestroyment,  (Vector3.up * 0.72f)+transform.position, Quaternion.identity) as GameObject;

			zappingParticleSystem.transform.parent = transform.parent;

			//zappingParticleSystem.transform.position = Vector3.zero;

			zapping.Play ();

			gameController.addPoints (1);
		}



	}
}
