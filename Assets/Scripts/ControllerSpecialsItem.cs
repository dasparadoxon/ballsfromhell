﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handels the actual collectible special item
/// 
/// </summary>

public class ControllerSpecialsItem : MonoBehaviour {

	/// <summary>
	/// The debug.
	/// </summary>
	public bool debug = true;

	/// <summary>
	/// The special object.
	/// </summary>
	public Special specialObject;
	/// <summary>
	/// The special controller.
	/// </summary>
	public ControllerSpecials specialController;

	float scaleFactor = 0;

	Timer enlargeTimer = null;

	float maxSize = 250;
	float enlargeSpeed = 5f;

	float enlargeTimerSpeed = 0.005f;

	void Start () {

		transform.localScale = new Vector3 (0, 0, 0);
	}

	void Update () {

		transform.Rotate (new Vector3 (0, 1, 0), Space.World);

		if (transform.localScale.x < maxSize && enlargeTimer == null) {
			
			enlargeTimer = Timer.Register (enlargeTimerSpeed, () => Enlarge (transform), isLooped: true);

			if (debug)
				Debug.Log ("Setting Enlarge Timer active");
		}

		if (transform.localScale.x > maxSize && enlargeTimer != null) {
			Timer.Cancel (enlargeTimer);

			enlargeTimer = null;

			if (debug)
				Debug.Log ("Canceling Enlarge Timer");
		}
			
	}


	void OnCollisionEnter(Collision collision) {

		if (debug)
			Debug.Log ("Special Collided with " + collision.collider.name);

		string nameOfCollisionObject = collision.collider.name;

		if (nameOfCollisionObject == "Bowl") {

			if (enlargeTimer != null) 
				Timer.Cancel (enlargeTimer);

			specialController.SpecialHasBeenCollectedByUser (specialObject);

			Destroy (this.gameObject);
		}

	}

	void Enlarge(Transform theTransformToUse){

		if (debug)
			Debug.Log ("Enlarge");

		theTransformToUse.localScale = Vector3.one * scaleFactor;

		scaleFactor += enlargeSpeed;
	}
}
