﻿using UnityEngine;
using System.Collections;

/**
 * Controlls the light when the fire particle generator burns *
 * 
 * Balls from Hell 0.1
 * 
 * */

public class controllerVillageRuins : MonoBehaviour
{

	public GameObject fireLight;

	controllerGame gameController;

	Light fireLightObject;



	void Start ()
	{

		GameObject gameObject =  GameObject.FindGameObjectWithTag ("GameController");

		gameController = gameObject.GetComponent<controllerGame> ();

		gameController.addBurningVillage ();

		fireLightObject = fireLight.GetComponent<Light> ();

		StartCoroutine (deleteLight ());
	
	}

	void Update ()
	{
	}

	IEnumerator deleteLight ()
	{
		if(gameController.levelMode)
			gameController.removeAllComets ();

		// let it burn
		if(gameController.levelMode)
			gameController.runningCanvas.SetActive (false);

		yield return new WaitForSeconds (3.4f);

		// dim the fire light slowly

		while (fireLightObject.intensity > 0.1f) {
			
			fireLightObject.intensity -= 0.1f;
			yield return new WaitForSeconds (0.005f);

		}

		// light wont be needed anymore

		Destroy (fireLight);

		yield return new WaitForSeconds (gameController.timeTillGameOver);

		if(gameController.levelMode)
			gameController.gameOver ();

		gameController.removeBurningVillage ();

	}

}
