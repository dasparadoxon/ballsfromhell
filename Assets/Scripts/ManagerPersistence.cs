using UnityEngine;
using System.Collections;

using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ManagerPersistence : MonoBehaviour {

	public bool debug = true;

	public static ManagerPersistence Instance;

	void Awake() {
		//Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER","yes");
		Instance = this;
		if (debug)
			Debug.Log ("Persistence Manager awoke.");
	}

	public void Save(IPersistence objectToSave) {
		BinaryFormatter formatter = new BinaryFormatter();

		Debug.Log ("Path to save serialized object : " + Application.persistentDataPath + "/" + objectToSave.FileName);

		FileStream file = File.Open(Application.persistentDataPath + "/" + objectToSave.FileName, FileMode.OpenOrCreate);
		formatter.Serialize(file, objectToSave);
		file.Close();
	}

	public System.Object Load(string nameOfFile) {

		if (debug)
			Debug.Log ("PersistenceManager is loading file : " + nameOfFile);

		var serializedObject = new System.Object();
		if(File.Exists(Application.persistentDataPath + "/" + nameOfFile)) {
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/" + nameOfFile, FileMode.Open);
			serializedObject = formatter.Deserialize(file);
			file.Close();
		}
		return serializedObject;
	}

	public bool Exists(string nameOfFile){

		if (File.Exists (Application.persistentDataPath + "/" + nameOfFile)) {
			if (debug)
				Debug.Log ("Persistence Manger detected file at : " + Application.persistentDataPath + "/" + nameOfFile);
			return true;
		}

		if (debug)
			Debug.Log ("Persistence Manager could not find file at : " + Application.persistentDataPath + "/" + nameOfFile);
		
		return false;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
