using UnityEngine;

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

enum HIGHSCOREENTRY { Playername = 0,Score = 1};

[System.Serializable]
public class Highscore :  IPersistence
{

	bool debug = false;

	private int numberOfEntries = 6;
	private List<List<object>> highscoreList;

	public const string NameOfFile = "highscore.dat";

	public string FileName {
		get {
			return NameOfFile;
		}
	}

	public Highscore ()
	{

		if (debug)
			Debug.Log ("HighScore Class instantiated");

		highscoreList = new List<List<object>> ();

		initializeEmptyHighscore ();

		//createRandomDefaultValues ();
	}

	private void initializeEmptyHighscore(){


		List<List<object>> randomhighscoreList = new List<List<object>> ();

		for (int i = 1; i < 7; i++) {

			int randomValue = i * 100;

			List<object> newEntry = new List<object> ();

			newEntry.Add ("Player" + i);
			newEntry.Add (randomValue);

			if (debug)
				Debug.Log ("Index:" + i + " | Player : " + newEntry [0] + " | Score : " + newEntry [1]);

			randomhighscoreList.Add (newEntry);

		}

		highscoreList = randomhighscoreList;
	}

	public void createRandomDefaultValues ()
	{

		if (debug)
			Debug.Log ("create Random Default Values...");

		List<List<object>> randomhighscoreList = new List<List<object>> ();

		for (int i = 1; i < 7; i++) {

			int randomValue = i * 10 * UnityEngine.Random.Range (0, 400);

			List<object> newEntry = new List<object> ();

			newEntry.Add ("Player" + i);
			newEntry.Add (randomValue);

			if (debug)
				Debug.Log ("Index:" + i + " | Player : " + newEntry [0] + " | Score : " + newEntry [1]);

			randomhighscoreList.Add (newEntry);

		}

		highscoreList = randomhighscoreList;

	}

	public Dictionary<string,int>[] getSortedHighscoreList ()
	{

		if (debug)
			Debug.Log ("Sorting Highscorelist...");

		Dictionary<string,int>[] result = new Dictionary<string, int>[6];

		int index = 0;

		foreach (List<object> entry in highscoreList.OrderByDescending(entry => entry[1])) {

			if(debug)
				Debug.Log ("index:" + index + " | " + entry [0] + "/" + entry [1]);

			Dictionary<string,int> newEntry = new Dictionary<string,int> ();

			newEntry.Add ((string)entry [0], (int)entry [1]);

			result [index] = newEntry;

			index++;

		}

		return result;

	}


	public int getPlaceForNewScore(int newScore){



		int placeForNewScore = -1;

		List<List<object>> placeSearchHighscoreList = highscoreList;

		List<object> newHighscoreEntry = new List<object> ();

		string name = "Dumdideldei";

		newHighscoreEntry.Add (name);
		newHighscoreEntry.Add (newScore);

		placeSearchHighscoreList.Add (newHighscoreEntry);

		int index = 0;

		foreach (List<object> entry in placeSearchHighscoreList.OrderByDescending(entry => entry[1])) {


			if (index != placeSearchHighscoreList.Count () - 1) {

				if ((int)entry[1] == newScore)
					placeForNewScore = index;

			}

			index++;
		}


		return placeForNewScore;



	}

	public int addHighscoreEntry (string name, int newScore)
	{

		int placeForNewScore = -1;

		List<List<object>> newHighscoreList = new  List<List<object>> ();

		List<object> newHighscoreEntry = new List<object> ();


		newHighscoreEntry.Add (name);
		newHighscoreEntry.Add (newScore);

		highscoreList.Add (newHighscoreEntry);

		int index = 0;

		foreach (List<object> entry in highscoreList.OrderByDescending(entry => entry[1])) {


			if (index != highscoreList.Count () - 1) {

				if ((int)entry[1] == newScore)
					placeForNewScore = index;

				newHighscoreList.Add (entry);

			}

			index++;
		}

		if (placeForNewScore != -1) {

			int reverseArrayIndexModifier = 6;

			placeForNewScore = reverseArrayIndexModifier - placeForNewScore;
		}

		highscoreList = newHighscoreList;

		return placeForNewScore;

	}

	public void changeNameAtSlot(int slot, string name){

		slot = 6 - slot;

		highscoreList[slot][0]= name;

	}

}

