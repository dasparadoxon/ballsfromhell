﻿using UnityEngine;
using System.Collections;

public class controllerFloor : MonoBehaviour
{

	[Header("Prefabs")]
	public GameObject cometDestroyment;

	[Header("Debug")]
	public GameObject verticesMarkDebug;
	public GameObject verticesMarkDebug2;

	[Header("Audio")]
	public AudioClip hellballBurstingOnFloor;

	[Header("System")]
	public GameObject fxContainer;

	MeshFilter meshFilter;
	Mesh mesh;

	Mesh orginalMesh;

	Vector3[] orginalVertices;

	controllerGame gameController;

	AudioSource audioSource;

	void Update ()
	{


	}

	public void randomizeMeshVertices ()
	{
		Vector3[] vertices;
		int[] triangles;

		vertices = meshFilter.mesh.vertices;

		int dim = 6;

		for (int y = 0; y < dim; y++)
			for (int x = 0; x < dim; x++) {

				Vector3 currentMeshVectorInWorldSpace = transform.TransformPoint (vertices [(y*dim)+x]);

				vertices [(y*dim)+x] = transform.InverseTransformPoint (

					new Vector3 (currentMeshVectorInWorldSpace.x, currentMeshVectorInWorldSpace.y + ((float)y * 2f), currentMeshVectorInWorldSpace.z)
				);
			}

		mesh.vertices = vertices;

		mesh.RecalculateBounds ();

		// Update Mesh Collider

		GetComponent<MeshCollider> ().sharedMesh = null;
		GetComponent<MeshCollider> ().sharedMesh = mesh;

	}

	public void colorFillMeshVertices ()
	{

		Color[] colors;

		colors = meshFilter.mesh.colors;

		for (int i = 0; i < meshFilter.mesh.vertices.Length; i++) {

			colors [i] = new Color (55.0f / 255.0f, 55.0f / 255.0f, 55.0f / 255.0f, 1);

		}

		// assign the array of colors to the Mesh.
		meshFilter.mesh.colors = colors;

	}

	public void restoreFloor ()
	{

		mesh.vertices = orginalVertices;

		mesh.RecalculateBounds ();

		// Update Mesh Collider

		GetComponent<MeshCollider> ().sharedMesh = null;
		GetComponent<MeshCollider> ().sharedMesh = mesh;

		colorFillMeshVertices ();
	}

	public void Start ()
	{

		GameObject gameObject = GameObject.FindGameObjectWithTag ("GameController");

		gameController = gameObject.GetComponent<controllerGame> ();

		meshFilter = GetComponent<MeshFilter> ();
		mesh = meshFilter.mesh;

		// Saving the orginal Mesh verticies to restore it later

		orginalMesh = meshFilter.mesh;

		orginalVertices = orginalMesh.vertices;

		colorFillMeshVertices ();

		Vector3[] verticies;
		int[] triangles;

		verticies = meshFilter.mesh.vertices;
		triangles = mesh.triangles;

		int numberOfVerticies = meshFilter.mesh.vertexCount;

	}

	public void OnCollisionEnter (Collision collision)
	{

		string collisionObjectName = collision.collider.gameObject.name;
		string collisionObjectTag = collision.collider.gameObject.tag;

		Vector3 positionToHit = collision.collider.transform.position;

		ContactPoint contact = collision.contacts [0];

		// Destroy the comet when it hits the floor

		if (collisionObjectTag == "Comet") {

			gameController.ballDestroyed ();

			Destroy (collision.collider.gameObject);

			GameObject ballDestroymentParticleSystem =  Instantiate(cometDestroyment, collision.collider.transform.position, Quaternion.identity) as GameObject;

			ballDestroymentParticleSystem.transform.parent = fxContainer.transform;

		}

		// Deform Terrain if needed

		if (collisionObjectTag == "Comet") {

			Vector3[] vertices = mesh.vertices;

			for (int i = 0; i < mesh.vertexCount; i++) {

				Vector3 currentMeshVectorInWorldSpace = transform.TransformPoint (vertices [i]);

				float area = 1.5f;

				if (positionToHit.x > currentMeshVectorInWorldSpace.x - area && positionToHit.x < currentMeshVectorInWorldSpace.x + area)
				if (positionToHit.z > currentMeshVectorInWorldSpace.z - area && positionToHit.z < currentMeshVectorInWorldSpace.z + area) {

					
					vertices [i] = transform.InverseTransformPoint (new Vector3 (currentMeshVectorInWorldSpace.x, currentMeshVectorInWorldSpace.y - 2f, currentMeshVectorInWorldSpace.z));

					Color[] colors;

					colors = meshFilter.mesh.colors;

					colors [i] = Color.yellow;

					meshFilter.mesh.colors = colors;


				}

			}

			mesh.vertices = vertices;

			mesh.RecalculateBounds ();

			// Update Mesh Collider

			GetComponent<MeshCollider> ().sharedMesh = null;
			GetComponent<MeshCollider> ().sharedMesh = mesh;

		}

	}

}
