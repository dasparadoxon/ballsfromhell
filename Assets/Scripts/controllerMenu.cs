﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class controllerMenu : MonoBehaviour {

	public bool debug = true;

	public ControllerApplication applicationController;

	public bool captureVideoScreenshots = false;

	int screenshotCounter = 0;

	float all25ms = 0f;

	void Start () {
	}
	

	void Update () {

		/*
		all25ms += Time.deltaTime;

		if (all25ms > 0.25f) {

			if (captureVideoScreenshots) {
				
				Application.CaptureScreenshot ("Screenshot" + screenshotCounter + ".png");
				screenshotCounter++;
			}

			all25ms -= 0.25f;

		}*/
	
	}

	public void startEndlessModeScene(){

		applicationController.levelMode = false;

		if (debug)
			Debug.Log ("Starting Endless Mode Game...");

		SceneManager.LoadScene(1); 

	}

	public void startLevelModeScene(){

		applicationController.levelMode = true;

		if (debug)
			Debug.Log ("Starting Level Mode Game...");

		SceneManager.LoadScene(1); 
	}

	public void switchToCreditsScene(){

		if (debug)
			Debug.Log ("Switching from Main Menu to Credits...");

		SceneManager.LoadScene(3); 

	}
}
