using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Slot Class - contains all data for an active slot
/// </summary>
public struct Slot
{

	/// <summary>
	/// The slot order.
	/// </summary>
	public int slotOrder;
	/// <summary>
	/// The name of the special.
	/// </summary>
	public string specialName;
	/// <summary>
	/// The special icon.
	/// </summary>
	public Image specialIcon;
	/// <summary>
	/// The count.
	/// </summary>
	public int count;
	/// <summary>
	/// The special class.
	/// </summary>
	public Special specialClass;
}

/// <summary>
/// Base SpecialsBar Class, consists of a variable number of Slot Objects
/// </summary>
public class SpecialsBar : Object
{

	/// <summary>
	/// The debug.
	/// </summary>
	public bool debug;

	/// <summary>
	/// The slots.
	/// </summary>
	public List<Slot> slots = new List<Slot> ();
	/// <summary>
	/// The nr of slots.
	/// </summary>
	public int nrOfSlots = 0;
	/// <summary>
	/// The special bar slots container.
	/// </summary>
	public GameObject specialBarSlotsContainer;

	/// <summary>
	/// The special counter text user interface elements.
	/// </summary>
	public Text[] specialCounterTextUIElements;
	/// <summary>
	/// The slot containers.
	/// </summary>
	public GameObject[] slotContainers;

	/// <summary>
	/// Initializes a new instance of the <see cref="SpecialsBar"/> class.
	/// </summary>
	/// <param name="specialBarSlotsContainerToUse">Special bar slots container to use.</param>
	public SpecialsBar (GameObject specialBarSlotsContainerToUse)
	{

		debug = false;

		specialBarSlotsContainer = specialBarSlotsContainerToUse;

		specialCounterTextUIElements = specialBarSlotsContainer.GetComponentsInChildren<Text> ();

		int numberOfSlots = specialBarSlotsContainer.transform.childCount;

		slotContainers = new GameObject[numberOfSlots];

		if (debug)
			Debug.Log ("Number of Slots : " + numberOfSlots);


		for (int i = 0; i < numberOfSlots; i++) {

			Transform slotTransform;

			slotTransform = specialBarSlotsContainer.transform.GetChild (i);

			if (debug)
				Debug.Log (slotTransform.gameObject);

			slotContainers [i] = slotTransform.gameObject;


		}


	}

	/// <summary>
	/// Reset this instance.
	/// </summary>
	public virtual void Reset ()
	{

		foreach (var item in slotContainers) {

			item.SetActive (false);
		}


		foreach (var item in specialCounterTextUIElements) {

			item.text = "0";
		}


	}

}

/// <summary>
/// Special Class for the minigame Balls From Hell derived from SpecialsBar
/// </summary>
public class BallsFromHellSpecialsBar : SpecialsBar
{

	/// <summary>
	/// Initializes a new instance of the <see cref="BallsFromHellSpecialsBar"/> class.
	/// </summary>
	/// <param name="specialBarSlotsContainerToUse">Special bar slots container to use.</param>
	public BallsFromHellSpecialsBar (GameObject specialBarSlotsContainerToUse) : base (specialBarSlotsContainerToUse)
	{
		nrOfSlots = 6;

		for (int slotIndex = 0; slotIndex < nrOfSlots; slotIndex++) {

			Slot slotToAdd = new Slot ();

			slotToAdd.slotOrder = slotIndex;
			slotToAdd.specialName = "";
			slotToAdd.specialIcon = null;
			slotToAdd.count = 0;
			slotToAdd.specialClass = null;

			slots.Add (slotToAdd);

		}
	}
}


