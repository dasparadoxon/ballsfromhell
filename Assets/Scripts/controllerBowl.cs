﻿using UnityEngine;
using System.Collections;

public class controllerBowl : MonoBehaviour {
	

	public float speed = 30.0F;
	public float upDownSpeed = 0.02f;

	public Camera bowlCamera;

	public bool active = false;

	Vector3 forwardsVector;
	Vector3 sidewardsVector;
	Vector3 forwardsNormalizedVector;

	void Start () {

		// the correct movement vectors are calculated with the line between camera 
		// and bowl position

		forwardsVector = transform.position - bowlCamera.transform.position;

		forwardsNormalizedVector = forwardsVector;
		forwardsNormalizedVector.Normalize ();
		forwardsNormalizedVector.y = 0f;

		sidewardsVector = Quaternion.AngleAxis(90, Vector3.up) * forwardsNormalizedVector;
	
	}

	void Update () {

		// this is used to prevent player movment during briefing etc.

		if(active)
			movement ();
	
	}

	void movement(){

		Vector3 oldPosition = transform.position;

		float translationY = Input.GetAxis("Mouse Y") * speed;
		float translationX = Input.GetAxis("Mouse X") * speed;

		translationY *= Time.deltaTime;
		translationX *= Time.deltaTime;

		float translationZ = 0;

		if (Input.GetButton ("MoveUp")) {

			translationZ = upDownSpeed * speed;
		}

		if (Input.GetButton ("MoveDown")) {

			translationZ = - upDownSpeed * speed;
		}


		Vector3 scaledTranslate = forwardsNormalizedVector * translationY;

		Vector3 scaledTranslateSidewards = sidewardsVector * translationX;

		transform.Translate(scaledTranslate.x,scaledTranslate.y,scaledTranslate.z,Space.World);

		transform.Translate(scaledTranslateSidewards.x,scaledTranslateSidewards.y,scaledTranslateSidewards.z,Space.World);

		transform.Translate(0,translationZ,0,Space.World);

		if (!checkBorders()) {

			transform.position = oldPosition;
		}



	}

	/**
	 * returns true if the new coordinates are with the play area
	 * 
	 */
	bool checkBorders(){

		Vector3 viewportPos = bowlCamera.WorldToViewportPoint (transform.position);

		if (viewportPos.x < 0f)
			return false;

		if (viewportPos.x > 1f)
			return false;

		if (viewportPos.z < 2.8f)
			return false;

		if (viewportPos.z > 55f)
			return false;

		// has to be done with real y 

		if (transform.position.y < 0.6200013f)
			return false;

		if (transform.position.y > 7.190005f)
			return false;


		return true;


	}

	float verticalMovement(){

		float mouseWheel = Input.GetAxis ("Mouse ScrollWheel");

		return mouseWheel;
	}
}
