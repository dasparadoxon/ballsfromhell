﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEditor;




public class controllerLightning : MonoBehaviour
{

	const int LIGHTING_MODE_LAYERS = 0;
	const int LIGHTING_MODE_LIGHTS = 1;

	[Header("Debugging")]

	public bool debug = false;

	[Header("Audio")]

	public AudioClip[] thunderAudios;

	[Space(10)]

	//[Header("Particle Systems")]
	//public Component[] ParticleSystem;

	[HideInInspector]
	public int lightingModeChoice = LIGHTING_MODE_LIGHTS;

	GameObject layerLightingLightningGameObject = null;

	layerLightingSystem landscapeLightingSystem;

	List<ThunderToLightning> lightnings;

	void CreateThunders ()
	{

		lightnings = new List<ThunderToLightning> ();

		ThunderToLightning newToLObject;

		ParticleSystem[] lightningPS;
		Transform[] lightningTransforms;

		lightningPS = GetComponentsInChildren<ParticleSystem> ();

		lightningTransforms = GetComponentsInChildren<Transform> ();

		int indexPSs = 0;



		// Connect Particle Systems to LightningSystem of the landscape

		foreach (ParticleSystem lPS in lightningPS) {

			if (debug)
				Debug.Log ("Creating Thunders for PS : " + indexPSs);

			newToLObject = new ThunderToLightning ();
			newToLObject.ps = lPS;
			newToLObject.initialPosition = lPS.transform.position;
			newToLObject.MovePSRandomlyOnOneAxis ();
			newToLObject.thunderSound = thunderAudios [0];

			if (lightingModeChoice == LIGHTING_MODE_LAYERS) {

				// terrible, TODO has to be made nice

				if (indexPSs == 0) {

					newToLObject.layerLightningSystem = landscapeLightingSystem;
					newToLObject.layerGroupName = "bigThunder";

				}

				if (indexPSs == 1) {

					newToLObject.layerLightningSystem = landscapeLightingSystem;
					newToLObject.layerGroupName = "smallThunder";

				}

				if (debug)
					Debug.Log ("Creating Layer Lightning System "+newToLObject.layerGroupName+" for PS : " + indexPSs);

			}

			//lPS.transform.gameObject.SetActive (false);

			lightnings.Add (newToLObject);

			indexPSs++;
		}
	}

	void Start ()
	{

		if (lightingModeChoice == LIGHTING_MODE_LAYERS) {

			// Getting the Landscape Lighting System to light the landscape layers when a lightning happens

			landscapeLightingSystem = layerLightingLightningGameObject.GetComponent<layerLightingSystem> ();

			if (landscapeLightingSystem == null)
				throw new UnityException ("The Landscape Lighting System is not initialized ?!");

			// Creating the LayerGroups

			LayerGroup layerGroupBigThunder = new LayerGroup ();
			LayerGroup layerGroupSmallThunder = new LayerGroup ();

			layerGroupBigThunder.name = "bigThunder";
			layerGroupBigThunder.layerNumbers = new List<int> () { 4, 5 };

			layerGroupSmallThunder.name = "smallThunder";
			layerGroupSmallThunder.layerNumbers = new List<int> { 3 };

			landscapeLightingSystem.layerGroups.Add (layerGroupBigThunder);
			landscapeLightingSystem.layerGroups.Add (layerGroupSmallThunder);

			// Create the particle systems and connect sound and landscape lighting

		}

		CreateThunders ();
	
	}

	void Update ()
	{

		foreach (ThunderToLightning tTL in lightnings) {

			tTL.Update ();
		}
	
	}

}

/*********************
 * Helper Class
 * */

public class ThunderToLightning
{

	const int LIGHTING_MODE_LAYERS = 0;
	const int LIGHTING_MODE_LIGHTS = 1;

	public int lightingMode = LIGHTING_MODE_LIGHTS;

	public bool debug = false;

	public AudioClip thunderSound;

	public ParticleSystem ps;
	public bool isActive;

	public string name;

	public string layerGroupName;
	public layerLightingSystem layerLightningSystem;

	public Vector3 initialPosition;

	public void Update ()
	{

		// play the (thunder) sound at the moment a particle is emited once

		if (ps.particleCount > 0 && !isActive) {

			isActive = true;
			AudioSource.PlayClipAtPoint (thunderSound, ps.transform.position);
			if(lightingMode == LIGHTING_MODE_LAYERS)
				layerLightningSystem.Fire (layerGroupName);
		}

		if (ps.particleCount < 1 && isActive) {
			isActive = false;
			MovePSRandomlyOnOneAxis ();
		}
	}

	public void MovePSRandomlyOnOneAxis ()
	{

		// Reset to initial position before moving

		ps.transform.position = initialPosition;

		// Move Particle Systems on the x-Axis randomly

		float movingRange = 13f;
		int transformIndex = 0;

		float randomPart = Random.Range (-movingRange, movingRange);

		if (debug)
			Debug.Log ("Moving Lightning System " + randomPart + " on the x-Axis");

		ps.transform.position = ps.transform.position + new Vector3 (randomPart, 0, 0);

	}

}



