﻿
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;



public class ControllerHighscore : MonoBehaviour {

	private bool debug = true;

	private Highscore highscores;

	/// <summary>
	/// The container highscore texts.
	/// </summary>
	public GameObject containerHighscoreTexts;
	/// <summary>
	/// The container highscore name texts.
	/// </summary>
	public GameObject containerHighscoreNameTexts;

	/// <summary>
	/// The persistence manager.
	/// </summary>
	public ManagerPersistence persistenceManager;

	/// <summary>
	/// The name of the input field high score.
	/// </summary>
	public InputField inputFieldHighScoreName;

	/// <summary>
	/// The application controller.
	/// </summary>
	public ControllerApplication applicationController;

	private InputField highScoreNameInputPrefabObjectInstance;

	private int numberOfHighscores = 6;

	private void initializeHighscoreArrays(){

	}

	private string generateHighscoreLine(int order,int[] highscoresToUse){

		return "";
	}

	private void setCurrentHighscores(){

		Text[] highscoreTextUIFields = containerHighscoreTexts.GetComponentsInChildren<Text>();
		Text[] highscoreNameTextUIFields = containerHighscoreNameTexts.GetComponentsInChildren<Text>();

		Dictionary<string,int>[] highscoreList = highscores.getSortedHighscoreList();

		for (int i = 0; i < 6; i++) {

			foreach (KeyValuePair<string, int> pair in highscoreList[i])
			{

				highscoreTextUIFields [i].text = pair.Value.ToString ();
				highscoreNameTextUIFields [i].text = pair.Key;
			}

		}

	}

	private int findNewHighscorePlace(int newScore){

		int index = 0;

		return index;
	}



	private void loadOrCreateHighScorePersistenceClass (){

		//highScorePersistenceClass = new HighScore ();

		loadHighscoreFromDisk ();

	}

	private void createHighscoreClass(){

		if (persistenceManager == null)
			throw new Exception ("The Persistence Manager Singelton seems not to be initialized ?!");

		if(persistenceManager.Exists("highscore.dat"))
			highscores = (Highscore)persistenceManager.Load ("highscore.dat");
		else
			highscores = new Highscore ();


	}

	void Start () {

		if (debug)
			Debug.Log ("Highscore Controller started...");

		// This is ugly, but works fine TODO


		applicationController = setApplicationControllerFromSingelton();

		createHighscoreClass ();

		setCurrentHighscores ();

		enterNewHighscore (getScoreFromMainGameLoop());
	
	}

	public ControllerApplication setApplicationControllerFromSingelton(){

		if (debug)
			Debug.Log ("Highscore Controller gets Application Controller from Singelton...");

		GameObject singeltonApplicationController =  GameObject.Find ("ApplicationController");

		ControllerApplication cApp = singeltonApplicationController.GetComponent<ControllerApplication> ();

		return cApp;
	}

	int getScoreFromMainGameLoop(){

		if (debug)
			Debug.Log ("Getting highscore from main game scene : " + applicationController.score);

		if (applicationController == null)
			throw new Exception ("Can not get highscore from Application Controller, its not instantiated");

		return applicationController.score;


	}

	void Update () {
	

		/*if (Input.anyKey ) {

			highscores.addHighscoreEntry ("XXX_XXX", UnityEngine.Random.Range(500,13500));

			setCurrentHighscores ();
		}*/
	}

	/// <summary>
	/// Enters the new highscore.
	/// </summary>
	/// <param name="newScore">New score.</param>
	public void enterNewHighscore(int newScore){

		if(debug)
			Debug.Log("Entering new HighScore : "+newScore);

		int placeForHighScore = highscores.addHighscoreEntry ("No Name",newScore);

		bool belowAllPreviousHighScores = false;

		if (placeForHighScore == -1)
			belowAllPreviousHighScores = true;

		if (debug)
			Debug.Log ("Place for new score :" + placeForHighScore);



		setCurrentHighscores ();

		if (!belowAllPreviousHighScores) {

			if (debug)
				Debug.Log ("High score is bigger than the lowest entry, thus added.");

			// get player name

			highScoreNameInputPrefabObjectInstance = GameObject.Instantiate (inputFieldHighScoreName) as InputField;

			// Get Position for the InputField

			Text[] highscoreNameTextUIFields = containerHighscoreNameTexts.GetComponentsInChildren<Text> ();

			int arrayIndexCorrection = 6;

			highScoreNameInputPrefabObjectInstance.transform.position = highscoreNameTextUIFields [arrayIndexCorrection - placeForHighScore].transform.position;

			highScoreNameInputPrefabObjectInstance.transform.parent = highscoreNameTextUIFields [arrayIndexCorrection - placeForHighScore].transform.parent;

			// connect the inputfields event when the user has finished editing by pressing enter

			highScoreNameInputPrefabObjectInstance.onEndEdit.AddListener (delegate {
				highScoreEditingFinishedByUser (highScoreNameInputPrefabObjectInstance.text, placeForHighScore);
			});


		} else
			if (debug)
				Debug.Log ("High score is lower than the lowest entry, thus NOT added.");;
	}


	/// <summary>
	/// Highs the score editing finished by user.
	/// </summary>
	/// <param name="name">Name.</param>
	/// <param name="placeForHighScore">Place for high score.</param>
	public void highScoreEditingFinishedByUser(string name, int placeForHighScore){

		if (debug)
			Debug.Log ("Highscore Editing of the name finished : " + name + " at place "+placeForHighScore);

		Destroy(highScoreNameInputPrefabObjectInstance.gameObject);

		highscores.changeNameAtSlot (placeForHighScore, name);

		setCurrentHighscores ();

		saveHighscoreToDisk ();
	}

	private void saveHighscoreToDisk(){

		persistenceManager.Save (highscores);

	}

	private void loadHighscoreFromDisk(){
	}

	public void returnToMainMenu(){

		if (debug)
			Debug.Log ("Returning from HighScore to Main Menu...");

		SceneManager.LoadScene(0); 
	}
}
