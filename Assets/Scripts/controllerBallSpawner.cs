﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/***
 * Ball Spawner Controller
 * 
 * Game Project "Balls from Hell"
 * 
 * https://bitbucket.org/dasparadoxon/ballsfromhell
 * 
 ***/

public class controllerBallSpawner : MonoBehaviour
{
	[Header("System")]

	public controllerGame gameController;
	public GameObject ballContainer;

	[Header("Forces")]
	public float leftDrall;
	public float rightDrall;

	public float force;
	public float forceDynamic;

	[Header("Options")]
	public float spawnIntervalInSeconds;

	[Header("Prefabs")]
	public GameObject prefabBallToSpawn;



	// PRIVATE

	private int numberOfBallsCreated = 0;

	private GameObject activeBall;

	private bool impulseGivenFlag;

	private IEnumerator coroutineSpawner;


	void Start ()
	{

		impulseGivenFlag = false;

		coroutineSpawner = createBalls ();
	
	}

	public void activate(){

		StartCoroutine (coroutineSpawner);
	}

	public void deactivate(){

		StopCoroutine (coroutineSpawner);
	}


	void Update ()
	{



	}

	void Work ()
	{

		if (!impulseGivenFlag) {

			createBall ();

			impulseGivenFlag = false;

		}
	}

	void createBall ()
	{

		activeBall = (GameObject)Instantiate (prefabBallToSpawn) as GameObject;

		activeBall.transform.position = transform.position;

		activeBall.transform.parent = ballContainer.transform;

		numberOfBallsCreated++;

		//activeBall.name = "BallFromHell_" + numberOfBallsCreated.ToString ();

		activeBall.name = "BallFromHell";

		Rigidbody ballRidigbody = activeBall.GetComponent<Rigidbody> ();
		
		float calculatedLeftDrall = Random.Range (-leftDrall, leftDrall);
		float calculatedRightDrall = Random.Range (-rightDrall, rightDrall);
		float calculatedForce = Random.Range (force - forceDynamic, force + forceDynamic);

		ballRidigbody.AddForce (
			calculatedLeftDrall,
			calculatedForce,
			calculatedRightDrall, 
			ForceMode.Impulse
		);

		gameController.ballCreated ();



	}


	public IEnumerator createBalls ()
	{
		while (true) {
			Work ();
			yield return new WaitForSeconds (spawnIntervalInSeconds);
		}
	}



}
