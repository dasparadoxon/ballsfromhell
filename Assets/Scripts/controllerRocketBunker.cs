﻿using UnityEngine;
using System.Collections;

public class controllerRocketBunker : MonoBehaviour {

	private Animator animator; 

	private animationRocketBunkerIsDone animationStateMachineBehaviour;

	void Awake ()
	{
		// Find a reference to the Animator component in Awake since it exists in the scene.
		animator = GetComponent <Animator> ();
	}

	// Use this for initialization
	void Start () {

		animationStateMachineBehaviour = animator.GetBehaviour <animationRocketBunkerIsDone> ();

		animationStateMachineBehaviour.rocketBunkerController = this;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
