﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * System for lighting a layer-set of images on planes
 * grouped inside a parent game object
 * 
 * made by das paradoxon 2016
 * 
 * originally developed for Balls From Hell
 * 
 * **/

public class LayerGroup
{

	public List<int> layerNumbers = new List<int> ();
	public string name;

}

public class layerLightingSystem : MonoBehaviour
{

	public bool debug = false;

	public List<Transform> layers = new List<Transform> ();

	public List<LayerGroup> layerGroups = new List<LayerGroup> ();

	void Start ()
	{

		Transform[] layersArray = GetComponentsInChildren<Transform> ();

		foreach (Transform layer in layersArray) {

			// first layer is the lighting system game object itself
			if (layer != layersArray [0])
				layers.Add (layer);
		}
	}

	void Update ()
	{
	
	}

	public void Fire (string layerGroupName)
	{

		if (debug)
			Debug.Log ("<b>Firing Layergroup : </b>" + layerGroupName);

		LayerGroup layerGroup;

		layerGroup = findLayer (layerGroupName);

		if (layerGroup != null) {

			foreach (int layerNumberToLight in layerGroup.layerNumbers) {

				if (debug)
					Debug.Log ("<i>   Firing Layer : </i>" + layerNumberToLight);

				lightLayer (layerNumberToLight);
			}
		}


	}

	LayerGroup findLayer (string layerGroupName)
	{

		foreach (LayerGroup layerGroup in layerGroups) {

			if (layerGroup.name == layerGroupName) {

				return layerGroup;
			}
		}

		return null;
	}

	void lightLayer (int indexOfLayerToLight)
	{

		if (debug)
			Debug.Log ("      Lighting Layer : " + indexOfLayerToLight);

		Texture texture;
		Transform layerTransform;
		GameObject layerGameObject;

		layerTransform = layers [indexOfLayerToLight];

		layerGameObject = layerTransform.gameObject;

		Renderer renderer = layerGameObject.GetComponent<Renderer> ();

		// enable material emission, @TODO this has to go into the constructor

		renderer.material.EnableKeyword ("_EMISSION");

		StartCoroutine (timedEffect (renderer));


	}

	IEnumerator timedEffect (Renderer render)
	{

		// this is a landscape layer lightning effect for the Game Hell Of Balls

		float emission = 0;

		float upperLimit = 0.2f;
		float bottomLimit = 0.0f;

		float emissionPartial = 0.05f;
		float timeIntervall = 0.05f;

		while (emission < upperLimit) {

			emission += emissionPartial;

			setEmission(render,new Color(emission,emission,emission,1));

			yield return new WaitForSeconds (timeIntervall);

		}

		while (emission > bottomLimit) {

			emission -= emissionPartial;

			setEmission(render,new Color(emission,emission,emission,1));

			yield return new WaitForSeconds (timeIntervall);

		}

	
	}


	void setEmission (Renderer renderer, Color emissionColor)
	{
		renderer.material.SetColor ("_EmissionColor", emissionColor);
	}
}
