﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Controller for missile created by the Rocket Special
/// </summary>

public class controllerMissile : MonoBehaviour
{

	public bool debug = false;

	public GameObject target;

	public float speed = 800f;
	public float speedSpeeder = 4f;
	public float step = 0f;

	public GameObject rocketBunker;

	Vector3 startPosition;

	void Start ()
	{

		startPosition = transform.position;
	
	}


	void Update ()
	{

		if (target != null) {
			
			transform.LookAt (target.transform);

			step += speed * Time.deltaTime * speedSpeeder;

			transform.position = Vector3.MoveTowards (startPosition, target.transform.position, step);


		} else {

			//Ball Target does not exist anymore...destroying Missile

			//Destroy (rocketBunker);

			Destroy (this.gameObject);
		}
			
	
	}

	void OnCollisionEnter (Collision collision)
	{

		GameObject collidedWith = collision.collider.gameObject;

		string nameOfABallObject = "BallFromHell";

		if (collidedWith.name == nameOfABallObject) {

			GameObject particleSystemExplosion = Instantiate (Resources.Load ("PS - MissileExplosion")) as GameObject;

			particleSystemExplosion.transform.position = transform.position;

			Destroy (collision.collider.gameObject);

			//Destroy (rocketBunker);
		}
	}

	public void SetTarget (GameObject missileTarget)
	{

		target = missileTarget;

	}
}
