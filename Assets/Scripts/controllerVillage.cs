﻿using UnityEngine;
using System.Collections;

public class controllerVillage : MonoBehaviour
{

	public GameObject villageRuinsPrefab;
	public GameObject villageFlamesParticleGenerator;

	public bool destroyable = true;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnTriggerEnter (Collider other)
	{

		if (other.tag == "Comet") {

			if (destroyable) {
		
				Destroy (this.gameObject);

				GameObject ruins = Instantiate (villageRuinsPrefab, transform.position, Quaternion.identity) as GameObject;
				GameObject flames = Instantiate (villageFlamesParticleGenerator, transform.position, Quaternion.identity) as GameObject;

				ruins.transform.Rotate (-90, 0, 0);

			}
		}
	}
}
