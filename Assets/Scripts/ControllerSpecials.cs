﻿/// compile with: /doc:BallsFromHellDocumentation.xml 

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// The Specials controller contains the SpecialBar and handles the use and activation of specials 
/// while the game is runnning
/// </summary>
public class ControllerSpecials : MonoBehaviour
{

	[Header ("System")]
	/// <summary>
	/// The game controller.
	/// </summary>
	public controllerGame gameController;
	/// <summary>
	/// The running user interface canvas.
	/// </summary>
	public Canvas runningUICanvas;

	public GameObject ballContainer;

	/// <summary>
	/// The special bar slots container.
	/// </summary>
	public GameObject specialBarSlotsContainer;

	[Header ("Spawning")]
	/// <summary>
	/// The center of specials spawn area.
	/// </summary>
	public GameObject centerOfSpecialsSpawnArea;
	/// <summary>
	/// The spawn dimension.
	/// </summary>
	public float spawnDimension = 14.0f;

	public GameObject containerForObjectsCreatedBySpecials;

	[Header ("Specials")]

	public GameObject playerBowl;

	[Header ("Debug")]
	/// <summary>
	/// The debug.
	/// </summary>
	public bool debug = true;

	public enum specialsSpawnModes // your custom enumeration
	{
		ALL,
		ROCKET,
		GROWBOWL,
		TIMESTOP}

	;

	public specialsSpawnModes specialsSpawnMode;

	/// <summary>
	/// A container for specials
	/// </summary>
	private List<Special> specials = new List<Special> ();

	/// <summary>
	/// The specials bar.
	/// </summary>
	private SpecialsBar specialsBar;

	Hashtable singleInstanceSpecialsBlockList = new Hashtable ();

	private List<Special> activeSpecials = new List<Special> ();

	void Start ()
	{

		specialsBar = new BallsFromHellSpecialsBar (specialBarSlotsContainer);

		specialsBar.Reset ();

		SpawnSpecial ();
	}

	void Update ()
	{

		// check if game is active and running ui is visible

		if (gameController.gameStatus () && runningUICanvas.isActiveAndEnabled) {

			CheckInputForSpecialActivation ();

		}

	
	}

	/// <summary>
	/// Adds the special.
	/// </summary>
	/// <param name="specialToAdd">Special to add.</param>
	public void AddSpecial (Special specialToAdd)
	{

		if (debug)
			Debug.Log ("Adding Special : " + specialToAdd.Name);

		specials.Add (specialToAdd);

	}

	/// <summary>
	/// Gets the specials.
	/// </summary>
	/// <returns>The specials.</returns>
	public List<Slot> getSpecials ()
	{

		return specialsBar.slots;

	}

	/// <summary>
	/// Sets the specials bar.
	/// </summary>
	/// <param name="specialsBarToUse">Specials bar to use.</param>
	public void setSpecialsBar (SpecialsBar specialsBarToUse)
	{

		specialsBar = specialsBarToUse;

	}

	/// <summary>
	/// Checks the User Input for Specials Activations
	/// </summary>
	public void CheckInputForSpecialActivation ()
	{



		if (Input.GetButtonDown ("Special1"))
			ActivateSpecial (1);

		if (Input.GetButtonDown ("Special2"))
			ActivateSpecial (2);

		if (Input.GetButtonDown ("Special3"))
			ActivateSpecial (3);

		if (Input.GetButtonDown ("Special4"))
			ActivateSpecial (4);

		if (Input.GetButtonDown ("Special5"))
			ActivateSpecial (5);

		if (Input.GetButtonDown ("Special6"))
			ActivateSpecial (6);
	}

	/// <summary>
	/// Activates a special in the by the player selected slot
	/// <param name="numerOfSlot">Specifies the number of the activated slot</param>
	/// </summary>
	public bool ActivateSpecial (int numberOfSlot)
	{

		// correcting offset

		numberOfSlot--;

		// check if there is a special in that slot

		int numberOfSpecialsInTheSlot = 0;
		Slot slotToUse;

		slotToUse = specialsBar.slots [numberOfSlot];

		numberOfSpecialsInTheSlot = slotToUse.count;

		if (numberOfSpecialsInTheSlot > 0) {
		
			//if( (!isSingleInstanceSpecialActive ("GROWBOWL") && slotToUse.specialClass.Name == "Bowl Grow Special") || slotToUse.specialClass.Name != "Bowl Grow Special"){

			//if( (!isSingleInstanceSpecialActive ("GROWBOWL") && slotToUse.specialClass.Name == "Bowl Grow Special") || slotToUse.specialClass.Name != "Bowl Grow Special"){

			bool allreadyActiveAndOnlyOneInstance = false;

			if (slotToUse.specialClass.onlyOneInstance)
				allreadyActiveAndOnlyOneInstance = checkIfSingleInstanceSpecialLikeThisIsActive (slotToUse.specialClass);

			if (!allreadyActiveAndOnlyOneInstance) {

				// insert the special in the activated specials list so special who can not be called again when they are active even if 
				// there are more than one of the type of special in the slot
				gameController.activeSpecials.Add (slotToUse.specialClass);

				if (debug)
					Debug.Log ("Activating Special in Slot : " + numberOfSlot.ToString () + ", in which there are " + slotToUse.count + " Specials");

				// if there is a special, decrement its count in the specialsBar

				slotToUse.count--;

				if (slotToUse.count > 0) {

					Text countUIText = specialsBar.slotContainers [numberOfSlot].GetComponentInChildren<Text> ();

					countUIText.text = slotToUse.count.ToString ();

					specialsBar.slots [numberOfSlot] = slotToUse;

				}

				if (slotToUse.count == 0) {

					specialsBar.slotContainers [numberOfSlot].SetActive (false);

					Slot resetedSlot = new Slot ();

					resetedSlot.slotOrder = slotToUse.slotOrder;

					specialsBar.slots [numberOfSlot] = resetedSlot;

				}
				//}

				// and execute the special by inserting it into the active array in the game controller



				return true;

			}

		}

		if (debug)
			Debug.Log ("The slot " + numberOfSlot + " is empty.");

		return false;
	}

	/// <summary>
	/// Spawns the special.
	/// </summary>
	public void SpawnSpecial ()
	{
		
		Special specialToSpawn = new Special ();

		int whichSpecial = Random.Range (1, 4);

		// TODO : use enum index directly

		if (specialsSpawnMode == specialsSpawnModes.ROCKET)
			whichSpecial = 1;

		if (specialsSpawnMode == specialsSpawnModes.TIMESTOP)
			whichSpecial = 2;

		if (specialsSpawnMode == specialsSpawnModes.GROWBOWL)
			whichSpecial = 3;

		if (debug)
			Debug.Log ("Spawning Special :" + whichSpecial);

		switch (whichSpecial) {
		case 1:
			specialToSpawn = new RocketSpecial ();
			specialToSpawn.Name = "Rocket Special";
			specialToSpawn.specialsController = this;
			specialToSpawn.specialGameObject = Instantiate (Resources.Load ("Rocket Special")) as GameObject;
			break;
		case 2:
			specialToSpawn = new TimeStopSpecial ();
			specialToSpawn.Name = "Time Stop Special";

			specialToSpawn.specialGameObject = Instantiate (Resources.Load ("Time Stop Special")) as GameObject;
			specialToSpawn.specialsController = this;
			break;
		case 3:

			bool isThereAnActiveGrowBowlSpecial = false;

			BowlGrowSpecial tBowlGrowSpecial = new BowlGrowSpecial ();


			tBowlGrowSpecial.Name = "Bowl Grow Special";
			tBowlGrowSpecial.specialGameObject = Instantiate (Resources.Load ("Bowl Grow Special")) as GameObject;
			tBowlGrowSpecial.bowl = playerBowl;

			specialToSpawn = tBowlGrowSpecial;
			specialToSpawn.specialsController = this;

			break;

		/*default:
			specialToSpawn = new RocketSpecial ();
			specialToSpawn.Name = "Rocket Special";
			specialToSpawn.specialGameObject = Instantiate (Resources.Load ("Rocket Special")) as GameObject;
			break;*/
		}

		specialToSpawn.gameController = gameController;

		// Position for Spawning
		float posX = Random.Range (-spawnDimension, spawnDimension);
		float posY = Random.Range (2, 7);
		float posZ = Random.Range (-spawnDimension, spawnDimension);

		// Setting Position 
		specialToSpawn.specialGameObject.transform.position = centerOfSpecialsSpawnArea.transform.position + new Vector3 (posX, posY, posZ);

		// Get Controller Script from the instantiated PreFab 
		ControllerSpecialsItem controllerSpecialsItemObject = specialToSpawn.specialGameObject.GetComponent<ControllerSpecialsItem> ();

		// set the Class and the Specials Controller in the component script from prefab
		controllerSpecialsItemObject.specialObject = specialToSpawn;
		controllerSpecialsItemObject.specialController = this;
	}

	public void addSpecialToActiveSpecialList (Special specialToAdd)
	{
		if(debug)
			Debug.Log ("Adding Special to active specials : " + specialToAdd.Name);	

		activeSpecials.Add (specialToAdd);
	}

	public void SpecialHasBeenCollectedByUser (Special collectedSpecial)
	{

		if (debug)
			Debug.Log ("Special Controller has received collected special : " + collectedSpecial.Name);

		// Check if there is allready a slot that contains one or more specials like the newly collected one

		bool slotExists = false;

		foreach (Slot slot in specialsBar.slots) {

			if (slot.count > 0) {

				if (slot.specialName == collectedSpecial.Name) {

					// If yes then increase the number of this special slot 

					slotExitsWithCollectedSpecials (slot.slotOrder);

					slotExists = true;
				}
			}
		}

		// If not find free slot and put it there

		if (!slotExists) {

			if (debug)
				Debug.Log ("Slot with specials of this kind does not exists");



			int numberOfSlotToUse = findFreeSlot ();

			// copy Slot

			Slot modifyInBarSlot = specialsBar.slots [numberOfSlotToUse];

			// change Slot

			modifyInBarSlot.specialName = collectedSpecial.Name;
			modifyInBarSlot.specialClass = collectedSpecial;
			modifyInBarSlot.count = 1;

			// write Slot back

			specialsBar.slots [numberOfSlotToUse] = modifyInBarSlot;

			// turn Slot visible in the UI

			specialsBar.slotContainers [numberOfSlotToUse].SetActive (true);

			// set the correct Special Icon in the Specials UI Bar

			Image icon = specialsBar.slotContainers [numberOfSlotToUse].GetComponentInChildren<Image> ();

			icon.overrideSprite = Resources.Load<Sprite> ("specialIcons/" + collectedSpecial.iconName);

			// set the count to one

			Text countUIText = specialsBar.slotContainers [numberOfSlotToUse].GetComponentInChildren<Text> ();

			countUIText.text = modifyInBarSlot.count.ToString ();

		}

		SpawnSpecial ();
	}

	void slotExitsWithCollectedSpecials (int slotNumber)
	{


		if (debug)
			Debug.Log ("Slot with specials of this kind exists, increasing slot count !");

		Slot modifyInBarSlot = specialsBar.slots [slotNumber];

		modifyInBarSlot.count++;

		specialsBar.slots [slotNumber] = modifyInBarSlot;

		Text countUIText = specialsBar.slotContainers [slotNumber].GetComponentInChildren<Text> ();

		countUIText.text = modifyInBarSlot.count.ToString ();

	}

	int findFreeSlot ()
	{

		if (debug)
			Debug.Log ("Searching for free Slot");

		Slot slotToReturn = new Slot ();

		slotToReturn.slotOrder = -1;

		foreach (Slot slot in specialsBar.slots) {

			if (slot.count == 0) {

				if (debug)
					Debug.Log ("Found free slot at slot number : " + slot.slotOrder);

				return slot.slotOrder;
			}
		}

		return -1;
	}

	public void setSingleInstanceSpecialActive (string name, bool status)
	{

		if (debug)
			Debug.Log ("Set Single Instance Special " + name + " to :" + status);

		if (singleInstanceSpecialsBlockList.ContainsKey (name)) {

			singleInstanceSpecialsBlockList [name] = status;

		} else
			singleInstanceSpecialsBlockList.Add (name, status);

	}

	public bool isSingleInstanceSpecialActive (string name)
	{

		if (debug)
			Debug.Log ("Searching for active Single Instance Special :" + name);

		if (singleInstanceSpecialsBlockList.ContainsKey (name)) {

			if (debug)
				Debug.Log ("Found Single Instance Special :" + name + " with status :" + singleInstanceSpecialsBlockList [name]);

			return (bool)singleInstanceSpecialsBlockList [name];
		}

		return false;

	}

	private bool checkIfSingleInstanceSpecialLikeThisIsActive (Special specialToCheckFor)
	{

		List<Special> listOfAllActiveSpecials = new List<Special> ();

		listOfAllActiveSpecials = gameController.activeSpecials;

		int index = 0;

		foreach (Special activeSpecial in listOfAllActiveSpecials) {

			if(debug)
				Debug.Log (index + " | " + activeSpecial.Name + " / " + specialToCheckFor.Name);

			if (specialToCheckFor.Name == activeSpecial.Name) {

				if(debug)
					Debug.Log ("This single instance special is allready active. ");
				return true;
			}

			index++;
		}

		if(debug)
			Debug.Log ("This single instance special is not active.");

		return false;
	}

}
