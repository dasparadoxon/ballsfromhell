﻿using UnityEngine;
using System.Collections;

/***
 * Ball Controller
 * 
 * Game Project "Balls from Hell"
 * 
 * https://bitbucket.org/dasparadoxon/ballsfromhell
 * 
 ***/

public class controllerBall : MonoBehaviour
{


	[Header ("Options")]
	public float levelUnderWhichTheObjectShouldDestroyItself = -6.0f;
	public float secondsUntilTheTargetCrossIsShown = 0.7f;

	[Header ("Prefabs")]
	public GameObject targetCrossPrefab;

	[Header ("Audio")]
	public AudioClip berstingSound;
	public AudioClip ejectingSound;

	GameObject instantiatedTargetCross;

	bool targetCrossActive;
	bool overVillage;
	bool spawnVillage;

	float timeSinceOverVillage = 0f;

	AudioSource audioSource;


	void Start ()
	{

		audioSource = GetComponent<AudioSource> ();
	
	}

	void Update ()
	{

		float height = transform.position.y;

		if (height < levelUnderWhichTheObjectShouldDestroyItself) {

			Destroy (this.gameObject);

		}

	}

	void FixedUpdate ()
	{
		manageTargetCross ();
	}

	void OnCollisionEnter (Collision collision)
	{

		string collisionObjectName = collision.collider.gameObject.name;
		string collisionObjectTag = collision.collider.gameObject.tag;

		if (collisionObjectName == "Floor") {

			AudioSource.PlayClipAtPoint (berstingSound, transform.position);
		}
	}

	void manageTargetCross ()
	{

		// Displays target cross over village

		RaycastHit hitInfo;

		if (Physics.Raycast (transform.position, Vector3.down, out hitInfo)) {


			if (hitInfo.collider.gameObject.tag == "Village") {

				if (!overVillage) {

					overVillage = true;

				}

				if (overVillage && !spawnVillage) {

					timeSinceOverVillage += Time.fixedDeltaTime;

				}

				if (!spawnVillage && overVillage && timeSinceOverVillage > secondsUntilTheTargetCrossIsShown) {

					spawnVillage = true;

					instantiatedTargetCross = Instantiate (targetCrossPrefab, hitInfo.collider.transform.position, Quaternion.identity) as GameObject;

					instantiatedTargetCross.transform.Rotate (-90, 0, 0);

				}

			}

			if (hitInfo.collider.gameObject.tag != "Village") {

				if (spawnVillage) {

					overVillage = false;
					spawnVillage = false;
					Destroy (instantiatedTargetCross);

				}
			}
		}
	}

	void OnDestroy ()
	{
		
		if (overVillage)
			Destroy (instantiatedTargetCross);

	}
}
