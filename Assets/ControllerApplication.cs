﻿using UnityEngine;
using System.Collections;

public class ControllerApplication : MonoBehaviour {

	public bool debug = true;

	public static ControllerApplication applicationController;

	public int score;
	public bool levelMode;
	public string playerName;

	void Awake() {

		if (applicationController == null) {

			if (debug)
				Debug.Log ("Application Controller does not exist, creating and making it persistent.");


			// Ugly hack to make sure other scenes can find the singelton... TODO
			this.gameObject.name = "ApplicationController";

			DontDestroyOnLoad (this.gameObject);
			applicationController = this;


		} else if(applicationController != this){

			if (debug) {
				Debug.Log ("Not destroyed static controller :");
				Debug.Log ("Level Mode :" + levelMode);
				Debug.Log ("Score : " + score);
			}


			if (debug)
				Debug.Log ("Application Controller allready exists, destroying this new instance.");

			Destroy (this.gameObject);

			//this = applicationController;

		}

	}


	void Start () {
	
	}
	

	void Update () {
	
	}
}
