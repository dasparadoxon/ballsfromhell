﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEditor;

[CustomEditor(typeof(controllerLightning))]
public class LightningPSEditor : CustomEditorBase {

	string[] lightingModeChoices = new [] { "Lights","Layers" };
	int lightingModeChoiceIndex = 1;

	public override void OnInspectorGUI ()
	{

		base.OnInspectorGUI();

		EditorGUILayout.Space ();

		EditorGUILayout.LabelField("Lighting System", EditorStyles.boldLabel);

		lightingModeChoiceIndex = EditorGUILayout.Popup(lightingModeChoiceIndex, lightingModeChoices);

		controllerLightning lightningController = target as controllerLightning;

		lightningController.lightingModeChoice = lightingModeChoiceIndex;

		EditorUtility.SetDirty(target);
	}

}
