﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class effectFlamer : MonoBehaviour {

	public LineRenderer lineRenderer;


	void Start () {

		lineRenderer = GetComponent<LineRenderer> ();

		List<Vector3> positions = new List<Vector3> ();

		for (int i = 0; i < 20; i++) {

			int yShift = 0;

			if (i % 2 == 1)
				yShift = 5;

			positions.Add (new Vector3 (-10 + i, yShift, 0));

		}

		lineRenderer.SetVertexCount(positions.Count);
		lineRenderer.SetPositions(positions.ToArray());
	
	}
	

	void Update () {
	
	}
}
